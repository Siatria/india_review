let bonusesInfo = document.querySelector('.bonuses__info')
let bonusesBtn = document.querySelector('.bonuses__more .show-all')

let charactInfo = document.querySelector('.characteristics__info')
let charactBtn = document.querySelector('.characteristics__more .show-all')

let aboutInfo = document.querySelector('.about__info')
let aboutBtn = document.querySelector('.about__more .show-all')

let providersInfo = document.querySelector('.providers__info')
let providersBtn = document.querySelector('.providers__more .show-all')

let bankingInfo = document.querySelector('.banking__info')
let bankingBtn = document.querySelector('.banking__more .show-all')

let slotsInfo = document.querySelector('.slots__info')
let slotsBtn = document.querySelector('.slots__more .show-all')

bonusesBtn.addEventListener('click', () => {
    bonusesInfo.classList.toggle('toggle-list');
    bonusesBtn.classList.toggle('toggle-arrow');
});

charactBtn.addEventListener('click', () => {
    charactInfo.classList.toggle('toggle-list');
    charactBtn.classList.toggle('toggle-arrow');
});

aboutBtn.addEventListener('click', () => {
    aboutInfo.classList.toggle('toggle-list');
    aboutBtn.classList.toggle('toggle-arrow');
});

providersBtn.addEventListener('click', () => {
    providersInfo.classList.toggle('toggle-list');
    providersBtn.classList.toggle('toggle-arrow');
});

bankingBtn.addEventListener('click', () => {
    bankingInfo.classList.toggle('toggle-list');
    bankingBtn.classList.toggle('toggle-arrow');
});

slotsBtn.addEventListener('click', () => {
    slotsInfo.classList.toggle('toggle-list');
    slotsBtn.classList.toggle('toggle-arrow');
});                                  

// Open-close FAQ
let details = document.querySelectorAll("details");
for(let i = 0; i < details.length; i++) {
    details[i].addEventListener("toggle", accordion);
}
function accordion(event) {
    if (!event.target.open) return;
    let details = event.target.parentNode.children;
    for(let i = 0; i < details.length; i++) {
        if (details[i].tagName !== "DETAILS" ||
            !details[i].hasAttribute('open') ||
            event.target === details[i]) {
            continue;
        }
        details[i].removeAttribute("open");
    }
}